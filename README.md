### Debian Linux

X	lnstalasi server Debian 10.x GNU/Linux

X	Konfigurasi  IP Addressing
-	ip address & dns configure

X	Konfigurasi firewall Linux
-	ufw -> allow,deny port

X	lnstalasi dan konfigurasi DNS Server
-	bind9

X	lnstalasi dan konfigurasi Web Server (http/https)
-	apache2 (http/https->openssl)
-	nginx (http)

X	lnstalasi dan konfigurasi Reverse Web Proxy
-	nginx (http)

X	lnstalasi dan konfigurasi Mail Server
-	webmail -> roundcube, dovecot, postfix, mariadb

X	lnstalasi dan konfigurasi FTP Server (ftp/fpts)
-	proftpd(http/https->openssl)

X	lnstalasi dan konfigurasi SSH Server
-	openssh

X	lnstalasi dan Konfigurasi DHCP Server
-	isc-dhcp-server --> mikrotik dhcp relay

### Microsoft Windows 10

X	lnstalasi Microsoft Windows Client

X	Konfigurasi IP dan Network adapter

X	Konfigurasi Wireless Network (WiFi )

### Mikrotik Router

X	Konfigurasi static route dan dynamic route (OSPF)

	Konfigurasi Virtual Local Area Network (VLAN)

X	Konfigurasi Network Address Translation (NAT)
-	masquerade

X	Konfigurasi Firewall
-	nat, filter rules

X	Konfigurasi DHCP Relay
-	dhcp server -> debian

X	Konfigurasi Access Point (AP)

### Cisco Router dan Cisco Switch Manageable

X	Konfigurasi interface (IP address)

X	Konfigurasi static route dan dynamic route

X	Konfigurasi Virtual Local Area Network (VLAN) dan VTP

X	Konfigurasi dynamic route (OSPF)

X	Konfigurasi NAT dan Server

